const { writeFileSync } = require('fs')

const columnSeparator = ';'

exports.write = (filename, data) => {
  if (!data.length) {
    return
  }
  const keys = Object.keys(data[0])

  const headline = keys.join(columnSeparator) + '\n'

  const contents = data
    .map(item =>
      keys
        .reduce((result, key) => (result.push(item[key]), result), [])
        .join(columnSeparator)
    )
    .join('\n')

  writeFileSync(filename, headline + contents)
}
