//@ts-check
const Excel = require('exceljs')

function addDataSheet(book, data) {
  const dataSheet = book.addWorksheet('Dane')
  const headerNames = Object.keys(data[0])
  dataSheet.columns = headerNames.map(key => ({ header: key, key, width: 20 }))
  dataSheet.addRows(data)
  const dateColumns = headerNames.filter(name => name.startsWith('Data'))

  for (let dateColumn of dateColumns) {
    dataSheet.getColumn(dateColumn).numFmt = 'dd.mm.yyyy hh:mm'
  }
  const timeColumn = headerNames.find(name => name.startsWith('Czas'))
  dataSheet.getColumn(timeColumn).numFmt = '[h]:mm'

  const firstRow = dataSheet.getRow(1)

  firstRow.alignment = { horizontal: 'center' }
  firstRow.font = { bold: true }
}

exports.write = (filename, data) => {
  const book = new Excel.Workbook()
  addDataSheet(book, data)

  book.xlsx.writeFile(filename)
}
