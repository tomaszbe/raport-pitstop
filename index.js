// @ts-check
const rms = require('rms-ts')
const moment = require('moment')
const { write } = require('./xlsx')
const credentials = require('./credentials')

function createItemMapper(branchName) {
  return function itemMapper({
    NumerZewnetrzny,
    DataRozpoczecia,
    RzeczywistaDataZakonczeniaSprawy,
    Id
  }) {
    return {
      Oddział: branchName,
      'Numer sprawy': NumerZewnetrzny,
      'Data rozpoczęcia': moment(DataRozpoczecia).toDate(),
      'Data zakończenia': moment(RzeczywistaDataZakonczeniaSprawy).toDate(),
      'Czas blokady': moment(RzeczywistaDataZakonczeniaSprawy).diff(
        DataRozpoczecia,
        'days',
        true
      )
    }
  }
}

async function getBranchResults({ name: branchName, value: branchId }) {
  const collection = rms.collection('SprawaEditDTO')

  collection.columns([
    'NumerZewnetrzny',
    'DataRozpoczecia',
    'RzeczywistaDataZakonczeniaSprawy'
  ])

  collection.basicFilters([
    { Name: 'OddzialId', Value: branchId, Operator: 'eq' },
    { Name: 'Stan', Value: '2', Operator: 'eq' },
    { Name: 'TypSprawyId', Value: '5', Operator: 'eq' },
    { Name: 'LiniaProduktowaId', Value: '4', Operator: 'neq' }
  ])

  const { and, field } = rms.filterBuilder

  const filter = and(
    field('DataDodania').is.greater.than.or.equal.to('2019-03-01T00:00:00'),
    field('BlokujeSamochod').is.true()
  )

  collection.filter(filter)

  let items = await collection.get()
  let mapped = items.map(createItemMapper(branchName))
  // write(`./out/${branchName}.csv`, items)
  return mapped
}

async function work() {
  await rms.signIn(credentials)

  //@ts-ignore
  const branches = require('./branches.json')

  const promises = []

  for (let branch of branches) {
    promises.push(getBranchResults(branch))
  }

  const results = await Promise.all(promises)

  const items = results.reduce((prev, current) => [...prev, ...current])
  write('./out/raport-pitstop.xlsx', items)
}

work()
